const Endpoints = {
  basicUrl: 'http://localhost:3001',
  userList(): string {
    return `${this.basicUrl}/userList/`;
  },
  singleKudos(id?: number): string {
    return `${this.basicUrl}/kudosList/${id || ''}`;
  },
  kudosPage(to: string | number, from?: string | number): string {
    return `${this.basicUrl}/kudosPage/${to}/${from + '/' || ''}`;
  },
};
export { Endpoints };
