export interface IRootState {
  counter: ICounter;
  user: IUser;
  overlay: IOverlay;
}
export interface ICounter {
  counter: number;
}

export interface IUser {
  name: string;
  age: number;
}

export interface IOverlay {
  globalLoading: boolean;
}
export interface IEndpoints {}
