import React, { FC, useState } from 'react';
import * as cs from 'classnames';

import grid from '../../assets/styles/grid.scss';
import form from '../../assets/styles/form.scss';

interface IPasswordButtonProps {
  passwordStatus?: boolean;
  placeholder?: string;
  name?: string;
}

export const PasswordButton: FC<IPasswordButtonProps> = ({ passwordStatus, placeholder, name }) => {
  const [showPassword, triggerPassword] = useState(passwordStatus || false);
  return (
    <div className={cs(form.form__input, form.password, grid.row)}>
      <input
        className={cs(form.form__field, grid.col)}
        type={showPassword ? 'text' : 'password'}
        name={name || 'password'}
        placeholder={placeholder || 'Password'}
      />
      <span
        className={cs(form.form__icon)}
        onClick={() => {
          triggerPassword(!showPassword);
        }}
      >
        <img src="/src/assets/icons/eye.svg" alt="Eye" />
      </span>
    </div>
  );
};
