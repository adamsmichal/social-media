import React, { FC } from 'react';
import * as cs from 'classnames';
import { useTransition, animated } from 'react-spring';

import style from './LoadingComponent.module.scss';

interface ILoadingComponentProps {
  modifer?: 'small' | 'big';
  status: boolean;
}

export const LoadingComponent: FC<ILoadingComponentProps> | any = ({ modifer, status }) => {
  const transitions = useTransition(status, null, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });
  return transitions.map(
    ({ item, key, props }) =>
      item && (
        <animated.section style={props} key={key} className={cs(style.loader)}>
          <div className={cs(style.loader__container, style[modifer || ''])}>
            <div className={cs(style.loader__item)}></div>
            <div className={cs(style.loader__item)}></div>
            <div className={cs(style.loader__item)}></div>
            <div className={cs(style.loader__item)}></div>
            <div className={cs(style.loader__item)}></div>
            <div className={cs(style.loader__item)}></div>
          </div>
        </animated.section>
      ),
  );
};
