import * as React from 'react';
import * as cs from 'classnames';
import { useHistory } from 'react-router-dom';

import style from './SignIn.module.scss';
import layout from '../../assets/styles/layout.scss';
import grid from '../../assets/styles/grid.scss';

export const SignInPage: React.FC = () => {
  let history = useHistory();
  const goToLoginIn = () => {
    history.replace('/login');
  };
  const goToRegister = () => {
    history.replace('/register');
  };
  return (
    <section className={cs(style.signInPage, layout.container, grid.row)}>
      <img className={cs(style.signInPage__logo)} src="/src/assets/icons/company-logo.svg" alt="company-logo" />
      <p className={cs(style.signInPage__title, grid.row)}>
        Welcome to <span className={cs(style.signInPage__company)}>Company.</span>
      </p>
      <button onClick={goToLoginIn} className={cs(style.signInPage__btn, style.signInPage__logIn, grid.col_12)}>
        Log in
      </button>
      <button onClick={goToRegister} className={cs(style.signInPage__btn, grid.col_12)}>
        Create Account
      </button>
      <p className={cs(style.signInPage__policy, grid.row)}>
        By tapping Continue, Create account, I agree to Company's Terms of Service, Payments Terms of Service, Privacy Policy, and Non-discrimination
        Policy.
      </p>
    </section>
  );
};
