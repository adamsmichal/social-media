import React, { FC } from 'react';
import * as cs from 'classnames';
import { useHistory } from 'react-router-dom';

import style from './LoginPage.module.scss';
import grid from '../../assets/styles/grid.scss';
import form from '../../assets/styles/form.scss';
import layout from '../../assets/styles/layout.scss';

import { PasswordButton } from '../../shared/c-PasswordInput/PasswordInput';

export const LoginPage: FC = () => {
  const history = useHistory();
  const validateForm = () => {
    history.replace('/homePage/');
  };
  return (
    <section className={cs(style.login, layout.container)}>
      <h1 className={cs(style.login__title)}>Login</h1>
      <form className={cs(form.form, grid.row)} onSubmit={validateForm} noValidate={true}>
        <fieldset className={cs(grid.row)}>
          <input className={cs(form.form__input, grid.col_12)} type="email" name="email" id="email" placeholder="yourname@company.com" />
        </fieldset>
        <fieldset className={cs(grid.row)}>
          <PasswordButton />
        </fieldset>
        <button className={cs(form.form__submit, grid.col_6)}>Go!</button>
      </form>
      <span className={cs(form.form__skip, style.login__forgot)}>
        <a href="#">Forgot password?</a>
      </span>
      <footer className={cs(style.login__footer)}>
        Don't have an account?
        <a
          href="#"
          onClick={() => {
            history.replace('/register');
          }}
        >
          Sign Up
        </a>
      </footer>
    </section>
  );
};
