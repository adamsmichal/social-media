import React, { FC } from 'react';
import * as cs from 'classnames';
import { useHistory } from 'react-router-dom';

import style from './PasswordStep.module.scss';
import grid from '../../../../assets/styles/grid.scss';
import form from '../../../../assets/styles/form.scss';
import { PasswordButton } from '../../../../shared/c-PasswordInput/PasswordInput';

export const PasswordStep: FC = () => {
  const history = useHistory();
  const validateForm = () => {
    history.replace('/register/step-4');
  };
  return (
    <article className={cs(style.step, grid.row)}>
      <h2 className={cs(style.step__title)}>Create a password</h2>
      <p className={cs(style.step__subtitle)}>Your password must include at least one symbol and be 8 or more characters long.</p>
      <form className={cs(form.form, grid.row)} onSubmit={validateForm} noValidate={true}>
        <fieldset className={cs(grid.row)}>
          <PasswordButton />
        </fieldset>
        <fieldset className={cs(grid.row)}>
          <PasswordButton placeholder="Re-Type Password" />
        </fieldset>
        <button className={cs(form.form__submit, grid.col_6)}>Next Step</button>
      </form>
    </article>
  );
};
