import React, { FC } from 'react';
import * as cs from 'classnames';
import { useHistory } from 'react-router-dom';

import style from './NotificationStep.module.scss';
import grid from '../../../../assets/styles/grid.scss';
import form from '../../../../assets/styles/form.scss';

export const NotificationStep: FC = () => {
  const history = useHistory();
  const validateForm = () => {
    history.replace('/login');
  };
  return (
    <article className={cs(style.step, grid.row)}>
      <h2 className={cs(style.step__title)}>Turn on notifications</h2>
      <p className={cs(style.step__subtitle, grid.col_12)}>Get the most out of Company by staying up to date with what's happening.</p>
      <form className={cs(form.form, grid.row)} onSubmit={validateForm} noValidate={true}>
        <button className={cs(form.form__submit, form.inner, grid.col_12)}>Allow Notifications</button>

        <button className={cs(form.form__skip)}>Skip for now</button>
      </form>
    </article>
  );
};
