import React, { FC } from 'react';
import * as cs from 'classnames';
import { useHistory } from 'react-router-dom';

import style from './EmailStep.module.scss';
import grid from '../../../../assets/styles/grid.scss';
import form from '../../../../assets/styles/form.scss';

export const EmailStep: FC = () => {
  const history = useHistory();
  const validateForm = () => {
    history.replace('/register/step-3');
  };
  return (
    <article className={cs(style.step)}>
      <h2 className={cs(style.step__title, grid.col_12)}>What's your email?</h2>
      <form className={cs(form.form, grid.row)} onSubmit={validateForm} noValidate={true}>
        <fieldset className={cs(grid.row)}>
          <input className={cs(form.form__input, grid.col_12)} type="email" name="email" id="email" placeholder="yourname@company.com" />
        </fieldset>
        <button className={cs(form.form__submit, grid.col_6)}>Next Step</button>
      </form>
    </article>
  );
};
