import React, { FC } from 'react';
import * as cs from 'classnames';
import { useHistory } from 'react-router-dom';

import style from './NameStep.module.scss';
import grid from '../../../../assets/styles/grid.scss';
import form from '../../../../assets/styles/form.scss';

export const NameStep: FC = () => {
  const history = useHistory();
  const validateForm = () => {
    history.replace('/register/step-2');
  };
  return (
    <article className={cs(style.step)}>
      <h2 className={cs(style.step__title)}>What's your name?</h2>
      <form className={cs(form.form, grid.row)} onSubmit={validateForm} noValidate={true}>
        <fieldset className={cs(grid.row)}>
          <input className={cs(form.form__input, grid.col_12)} type="text" name="name" id="name" placeholder="First Name" />
        </fieldset>
        <fieldset className={cs(grid.row)}>
          <input className={cs(form.form__input, grid.col_12)} type="text" name="surname" id="surname" placeholder="Last Name" />
        </fieldset>
        <button className={cs(form.form__submit, grid.col_6)}>Next Step</button>
      </form>
    </article>
  );
};
