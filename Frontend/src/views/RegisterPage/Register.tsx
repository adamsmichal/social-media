import * as React from 'react';
import * as cs from 'classnames';
import { Switch, Route, Redirect, useRouteMatch, useHistory } from 'react-router-dom';
import style from './Register.module.scss';
import layout from '../../assets/styles/layout.scss';
import grid from '../../assets/styles/grid.scss';

import { NotificationStep } from './components/c-NotificationStep/NotificationStep';
import { NameStep } from './components/c-NameStep/NameStep';
import { EmailStep } from './components/c-EmailStep/EmailStep';
import { PasswordStep } from './components/c-PasswordStep/PasswordStep';

export const RegisterPage: React.FC = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  return (
    <section className={cs(style.registerPage, layout.container, grid.row)}>
      <nav className={cs(grid.row, style.registerPage__navi)}>
        <div
          className={cs(grid.col_1)}
          onClick={() => {
            history.goBack();
          }}
        >
          <img src="/src/assets/icons/back.svg" alt="Back" />
        </div>
      </nav>

      <Switch>
        <Route exact path="/register">
          <Redirect to={`${path}/step-1`} />
        </Route>
        <Route path={`${path}/step-1`} component={NameStep} />
        <Route path={`${path}/step-2`} component={EmailStep} />
        <Route path={`${path}/step-3`} component={PasswordStep} />
        <Route path={`${path}/step-4`} component={NotificationStep} />
      </Switch>
    </section>
  );
};
