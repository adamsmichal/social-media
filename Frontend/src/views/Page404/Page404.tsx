import * as React from 'react';
import * as cs from 'classnames';

import style from './Page404.module.scss';
import layout from '../../assets/styles/layout.scss';
import grid from '../../assets/styles/grid.scss';

export const Page404: React.FC = () => {
  return (
    <article className={cs(style.page404, layout.container)}>
      <p className={cs(style.page404__title)}>Oops!</p>
      <p className={cs(style.page404__error)}>404 - Page not found</p>
      <p className={cs(style.page404__info)}>
        The page you are looking for might have been removed, had its name changed or is temporarily unavailable
      </p>
      <button className={cs(style.page404__btn, grid.col_6)}>Go to homepage</button>
    </article>
  );
};
