import * as React from 'react';
import * as cs from 'classnames';
import { useLocation } from 'react-router';
import { useHistory } from 'react-router-dom';

import Style from './MinorNav.module.scss';

interface IPathTitle {
  path: string;
  title: string;
}

const titles: IPathTitle[] = [{ path: '/homePage/message', title: 'messages' }];

export const MinorNav: React.FC = () => {
  const getTitle = (): string => {
    const location = useLocation();
    const index: number = titles.findIndex((el: IPathTitle) => el.path === location.pathname);
    return titles[index].title;
  };

  let history = useHistory();

  return (
    <nav className={cs(Style.navigation)}>
      <img
        onClick={() => {
          history.goBack();
        }}
        className={cs(Style.navigation__icon)}
        src="/src/assets/icons/back.svg"
        alt="back-arrow"
      />
      <p className={cs(Style.navigation__title)}>{getTitle()}</p>
      <img className={cs(Style.navigation__avatar)} src="/src/assets/img/avatar.svg" alt="avatar" />
    </nav>
  );
};
