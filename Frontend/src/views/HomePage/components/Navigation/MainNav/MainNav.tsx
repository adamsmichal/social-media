import * as React from 'react';
import * as cs from 'classnames';
import { useHistory } from 'react-router-dom';

import Style from './MainNav.module.scss';

export const MainNav: React.FC = () => {
  let history = useHistory();
  const goToMessage = () => {
    history.push('/homePage/message');
  };
  return (
    <nav className={cs(Style.navigation)}>
      <img onClick={goToMessage} className={cs(Style.navigation__icon)} src="src/assets/icons/message.svg" alt="message" />
      <p className={cs(Style.navigation__title)}>logo</p>
      <img className={cs(Style.navigation__avatar)} src="src/assets/img/avatar.svg" alt="avatar" />
    </nav>
  );
};
