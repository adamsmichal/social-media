import * as React from 'react';
import * as cs from 'classnames';
import Slider from 'react-slick';

import style from './Slider.module.scss';

interface IGallerySliderProps {
  name: string;
  place: string;
  avatar: string;
}

const temporaryObject = [
  { title: 'Landscape', url: 'https://i0.wp.com/digital-photography-school.com/wp-content/uploads/2012/10/image1.jpg?w=600&h=1260&ssl=1' },
  { title: 'Landscape2', url: 'https://i0.wp.com/digital-photography-school.com/wp-content/uploads/2012/10/image1.jpg?w=600&h=1260&ssl=1' },
];

export const GallerySlider: React.FC<IGallerySliderProps> = ({ name, place, avatar }) => {
  const sliderOptions = {
    arrows: false,
    infinite: false,
    slidesToShow: 2.5,
    slidesToScroll: 2,
  };

  return (
    <article className={cs(style.slider)}>
      <div className={cs(style.slider__nav)}>
        <img className={cs(style.slider__avatar)} src={avatar} alt={name} />
        <div className={cs(style.slider__text)}>
          <p className={cs(style.slider__name)}>{name}</p>
          <p className={cs(style.slider__place)}>{place}</p>
        </div>
        <img className={cs(style.slider__menu)} src="src/assets/icons/dots-menu.svg" alt="dots" />
      </div>
      <Slider className={cs(style.slider__container)} {...sliderOptions}>
        {temporaryObject.map((el) => (
          <div>
            <img className={cs(style.slider__element)} key={el.title} src={el.url} />
          </div>
        ))}
      </Slider>
    </article>
  );
};
