import * as React from 'react';
import * as cs from 'classnames';
import { useLocation } from 'react-router';

import style from './Recomendation.module.scss';

import { RecomendationItem } from './RecomendationItem/RecomendationItem';

interface IPathTitle {
  path: string;
  title: string;
}

const titles: IPathTitle[] = [
  { path: '/homePage', title: 'Recomendation' },
  { path: '/homePage/message', title: 'Active' },
];

export const Recomendation: React.FC = () => {
  const getTitle = (): string => {
    const location = useLocation();
    const index: number = titles.findIndex((el: IPathTitle) => el.path === location.pathname);
    return titles[index].title;
  };

  return (
    <div className={cs(style.recomendation)}>
      <p className={cs(style.recomendation__title)}>{getTitle()}</p>
      <div className={cs(style.recomendation__container)}>
        <RecomendationItem url={'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png'} name={'Michał'} />
        <RecomendationItem url={'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png'} name={'Michał'} />
        <RecomendationItem url={'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png'} name={'Michał'} />
        <RecomendationItem url={'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png'} name={'Michał'} />
      </div>
    </div>
  );
};
