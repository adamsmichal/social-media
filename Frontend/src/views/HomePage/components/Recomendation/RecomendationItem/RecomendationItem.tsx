import * as React from 'react';
import * as cs from 'classnames';

import style from './RecomendationItem.module.scss';

interface IRecomendationItemProps {
  url: string;
  name: string;
}

export const RecomendationItem: React.FC<IRecomendationItemProps> = ({ url, name }) => {
  return (
    <div className={cs(style.recomendationItem)}>
      <div className={cs(style.recomendationItem__border)}>
        <img className={cs(style.recomendationItem__avatar)} src={url} alt="avatar" />
      </div>
      <p className={cs(style.recomendationItem__name)}>{name}</p>
    </div>
  );
};
