import * as React from 'react';
import * as cs from 'classnames';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { Recomendation } from './components/Recomendation/Recomendation';
import { MainNav } from './components/Navigation/MainNav/MainNav';
import { MinorNav } from './components/Navigation/MinorNav/MinorNav';

import style from './HomePage.module.scss';

export const HomePage: React.FC = () => {
  return (
    <Router>
      <section className={cs(style.homePage)}>
        <Switch>
          <Route exact path="/homePage" component={MainNav} />
          <Route exact path="/homePage/message" component={MinorNav} />
        </Switch>
        <Switch>
          <Route exact path={['/homePage', '/homePage/message']} component={Recomendation} />
        </Switch>
      </section>
    </Router>
  );
};
