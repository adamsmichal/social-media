import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IUser } from '../../model/interfaces/interfaces';

const initialState: IUser = {
  name: '',
  age: 0,
};

type ISetName = string;

const userSlice = createSlice({
  name: 'userSlice',
  initialState,
  reducers: {
    setName(state, action: PayloadAction<ISetName>) {
      state.name = action.payload;
    },
  },
});

export const { setName } = userSlice.actions;

export default userSlice.reducer;
