import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type TCounterState = number;

const initialState: TCounterState = 0;

type IIncrementCounter = number;
type IDecrementCounter = number;

const counterSlice = createSlice({
  name: 'counterSlice',
  initialState,
  reducers: {
    incrementCounter(_state, action: PayloadAction<IIncrementCounter>) {
      _state += action.payload ? action.payload : 1;
    },
    decrementCounter(_state, action: PayloadAction<IDecrementCounter>) {
      _state -= action.payload ? action.payload : 1;
    },
    resetCounter(_state) {
      _state = 0;
    },
  },
});

export const { incrementCounter, decrementCounter, resetCounter } = counterSlice.actions;

export default counterSlice.reducer;
