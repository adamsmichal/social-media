import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IOverlay } from '../../model/interfaces/interfaces';

const initialState: IOverlay = {
  globalLoading: false,
};

type TOverlayStatus = boolean;

const overlayReducer = createSlice({
  name: 'overlayReducer',
  initialState,
  reducers: {
    globalLoading(_state, action: PayloadAction<TOverlayStatus>) {
      _state.globalLoading = action.payload;
    },
  },
});

export const { globalLoading } = overlayReducer.actions;

export default overlayReducer.reducer;
