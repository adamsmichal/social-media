import { combineReducers } from '@reduxjs/toolkit';
import counterReducer from './reducers/counterReducer';
import userReducer from './reducers/userReducers';
import overlayReducer from './reducers/overlayReducer';

const rootReducer = combineReducers({
  counter: counterReducer,
  user: userReducer,
  overlay: overlayReducer,
});

export default rootReducer;
