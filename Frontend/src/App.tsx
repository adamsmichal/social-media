import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import * as cs from 'classnames';
import { useSelector } from 'react-redux';

import { RegisterPage } from './views/RegisterPage/Register';
import { SignInPage } from './views/SignInPage/SignIn';
import { HomePage } from './views/HomePage/HomePage';
import { LoginPage } from './views/LoginPage/LoginPage';

// global styles
import './assets/styles/normalize.scss';
import './assets/styles/base.scss';
import './assets/styles/fonts.scss';
import style from './App.module.scss';
import { LoadingComponent } from './shared/c-Loading/LoadingComponent';
import { IRootState } from './model/interfaces/interfaces';

const App: React.FC = () => {
  const { globalLoading } = useSelector((state: IRootState) => state.overlay);
  return (
    <Router>
      <main className={cs(style.app)}>
        <LoadingComponent modifer="big" status={globalLoading} />

        <Switch>
          <Route exact path="/" component={SignInPage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/homePage" component={HomePage} />
          <Route path="/login" component={LoginPage} />
        </Switch>
      </main>
    </Router>
  );
};

export default hot(module)(App);
